package ai.bank.Models;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="SAVEDFILES")
public class LoadFile {

		private String filename;
	    private String fileType;
	    private String fileSize;
	    private byte[] file;
	    
	   private String templateName;

	    public String getTemplateName() {
	    	return templateName;
	    }

	    public void setTemplateName(String templateName) {
	    	this.templateName = templateName;
		}

		public LoadFile() {
	    }

	    public String getFilename() {
	        return filename;
	    }

	    public void setFilename(String filename) {
	        this.filename = filename;
	    }

	    public String getFileType() {
	        return fileType;
	    }

	    public void setFileType(String fileType) {
	        this.fileType = fileType;
	    }

	    public String getFileSize() {
	        return fileSize;
	    }

	    public void setFileSize(String fileSize) {
	        this.fileSize = fileSize;
	    }

	    public byte[] getFile() {
	        return file;
	    }

	    public void setFile(byte[] file) {
	        this.file = file;
	    }
	
}
