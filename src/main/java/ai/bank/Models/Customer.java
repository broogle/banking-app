package ai.bank.Models;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection="CUSTOMER")
public class Customer extends Base{
	
	
	private static final long serialVersionUID = 1L;

	@Field(name="FIRSTNAME")
	private String firstName;
	
	@Field(name="MIDDLENAME")
	private String middleName;
	
	@Field(name="LASTNAME")
	private String lastName;
	
	@Field(name="ACCOUNTNUMBER")
	private String accountNumber;
	
	@Field(name="MOBILENUMBER")
	private String mobileNumber;
	
	@Field(name="BALANCE")
	private Double balance;
	
	@Field(name="IFSC")
	private String ifsc;
	
	@Field(name="EMAIL")
	private String email;
	
	@Field(name="ADDRESS")
	private String address;
	
	@Field(name="DEBITCARD")
	private String debitCard;
	
	@Field(name="CREDITCARD")
	private String creditCard;
	
	@Field(name="ACCOUNTTYPE")
	private String accountType;
	
	@Field(name="CREDITLIMIT")
	private String creditLimit;
	
	@Field(name="CREITSCORE")
	private String creditScore;
	
	@Field(name="AADHARNUMBER")
	private String aadharNumber;
	
	@Field(name="PANNUMBER")
	private String panNumber;
	

	
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getIfsc() {
		return ifsc;
	}
	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDebitCard() {
		return debitCard;
	}
	public void setDebitCard(String debitCard) {
		this.debitCard = debitCard;
	}
	public String getCreditCard() {
		return creditCard;
	}
	public void setCreditCard(String creditCard) {
		this.creditCard = creditCard;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getCreditLimit() {
		return creditLimit;
	}
	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}
	public String getCreditScore() {
		return creditScore;
	}
	public void setCreditScore(String creditScore) {
		this.creditScore = creditScore;
	}
	public String getAadharNumber() {
		return aadharNumber;
	}
	public void setAadharNumber(String aadharNumber) {
		this.aadharNumber = aadharNumber;
	}
	public String getPanNumber() {
		return panNumber;
	}
	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
}
