package ai.bank.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.bank.Models.Customer;
import ai.bank.Repository.CustomerRepository;
import ai.bank.Service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;


	@Override
	public Customer saveOrUpdate(String loginId, Customer entity) {
		return customerRepository.save(entity);
	}

	@Override
	public List<Customer> getByAccountNumber(String AccountNumber) {
		List<Customer> custId = customerRepository.findByAccountNumber(AccountNumber);
		return custId;
	}

	@Override
	public Customer getPrimaryKey(Long Id) {
		// TODO Auto-generated method stub
		return null;
	}


}
