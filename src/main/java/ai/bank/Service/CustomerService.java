package ai.bank.Service;

import java.util.List;

import ai.bank.Models.Customer;

public interface CustomerService {

	public Customer getPrimaryKey(Long Id);
	
	public List<Customer> getByAccountNumber(String accountNumber);
	
	public Customer saveOrUpdate(String loginId, Customer entity) ;
	
	
	
}
