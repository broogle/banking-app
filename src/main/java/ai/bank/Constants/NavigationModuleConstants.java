package ai.bank.Constants;

public class NavigationModuleConstants {

	public static final String GET_ALL = "/get";
	public static final String CREATE_CUSTOMER = "/create";
}
