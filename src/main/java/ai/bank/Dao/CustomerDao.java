package ai.bank.Dao;

import java.util.List;

import ai.bank.Models.Customer;

public interface CustomerDao {

	public List<Customer> getByAccountNumber(String accountNumber);
}
