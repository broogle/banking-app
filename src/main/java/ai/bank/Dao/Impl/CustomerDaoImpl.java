package ai.bank.Dao.Impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import ai.bank.Dao.CustomerDao;
import ai.bank.Models.Customer;
import ai.bank.Repository.CustomerRepository;

public class CustomerDaoImpl implements CustomerDao{

	@Autowired
	CustomerRepository customerRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(CustomerDaoImpl.class);
	
	@Override
	public List<Customer> getByAccountNumber(String accountNumber) {
		
		logger.debug("Inside getByAccountNumber  method ");
		logger.info("CustomerId= " + accountNumber);
		List<Customer> rMaaz = customerRepository.findByAccountNumber(accountNumber);
		if (null == rMaaz || rMaaz.size() == 0) {
			logger.info("Customer not found for {}", accountNumber);
			return null;
		}
		logger.debug("list found for accountNumber {}", accountNumber);
		return rMaaz;
		
	}

	
	
}
