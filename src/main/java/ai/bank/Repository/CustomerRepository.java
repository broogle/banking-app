package ai.bank.Repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import ai.bank.Models.Customer;

public interface CustomerRepository extends MongoRepository <Customer, String>{
	
	public List<Customer> findByAccountNumber(String accountNumber);

	
}