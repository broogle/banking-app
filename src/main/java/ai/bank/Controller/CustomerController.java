package ai.bank.Controller;

import ai.bank.Constants.NavigationModuleConstants;
import ai.bank.Constants.UrlMappingConstants;
import ai.bank.Models.Customer;
import ai.bank.Repository.CustomerRepository;
import ai.bank.Service.CustomerService;
import ai.bank.web.dto.CustomerVO;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping(UrlMappingConstants.CUSTOMER_CONTROLLER)
public class CustomerController {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private CustomerService customerService;

	private static Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);
	
	@ResponseBody
	@PostMapping(NavigationModuleConstants.CREATE_CUSTOMER)
	public String createCustomer(@RequestBody CustomerVO customer) throws ParseException {

		LOGGER.info("Entered createCustomer method");

		List<Customer> customerList = customerService.getByAccountNumber(customer.getAccountNumber());
		if (null != customerList && customerList.size() > 0) {
			LOGGER.info("Record already exists");
			return "Failure.. Record already exists";
		} else {
			
			
			Customer customerMst = new Customer();
			String pattern = "yyyy-MM-dd";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			String date = simpleDateFormat.format(new Date());
			
			
			Date dt = new Date();
			Mapper mapper= new DozerBeanMapper();
			mapper.map(customer, customerMst);
			customerMst.setCreatedDate(date);
			LOGGER.info("Data is mapped in CustomerMst");
			Customer customerMstNew = customerService.saveOrUpdate("login", customerMst);
			if (null != customerMstNew) {
				LOGGER.info("Record saved succesfully");
				return "Successfully saved record";
			} else {
				LOGGER.info("Failure while saving record");
				return "Failure while saving record";
			}
		}
	}

	@GetMapping(NavigationModuleConstants.GET_ALL)
	public ResponseEntity<?> GetCustomer() {

		LOGGER.info("Service has entered GetCustomer()");
		return ResponseEntity.ok(this.customerRepository.findAll());
	}


}

